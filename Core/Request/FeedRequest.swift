//
//  FeedRequest.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 02/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Moya

enum FeedType: String {
    case new
    case rising
    case best
    case hot
    case random
}

struct FeedRequest: NetworkingProtocol {
    
    typealias MappingModel = FeedRoot
    
    var type: FeedType
    var limit: Int
    var next: String?
    
    var path: String {
        return type.rawValue.appending(".json")
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var parameters: [String: Any] {
        var dict: [String: Any] = defaultParameters
        dict["limit"] = limit
        dict["after"] = next
        return dict
    }
    
    var task: Moya.Task {
        return .requestParameters(parameters: parameters, encoding: parameterEncoding)
    }
}
