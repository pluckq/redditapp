//
//  NetworkingProtocol.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 17/08/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Moya

protocol Mapable {
    associatedtype MappingModel: Codable
}

protocol NetworkingProtocol: Mapable, TargetType {
    
    var baseURL: URL { get }
    
    var path: String { get }
    
    var url: String { get }
    
    var headers: [String: String]? { get }
    
    var defaultParameters: [String: Any] { get }
    
    var method: Moya.Method { get }
    
    var task: Moya.Task { get }
    
    var parameterEncoding: Moya.ParameterEncoding { get }
    
    var sampleData: Data { get }
}

extension NetworkingProtocol {
    
    var baseURL: URL {
        return URL(string: "https://www.reddit.com/")!
    }
    
    var url: String {
        return baseURL.appendingPathComponent(path).absoluteString
    }
    
    var headers: [String: String]? {
//        var params: [String : String] = [:]
//        if let token = cachedToken {
//            params["Authorization"] = "Token \(token)"
//        } else if let token = token {
//            params["Authorization"] = "Token \(token)"
//        }
        return [:]
    }
    
    /*
     removes legacy json symbols from json, should be used in every request
     e.g.: .requestParameters(parameters: defaultParameters, encoding: parameterEncoding)
     */
    var defaultParameters: [String: Any] {
        return ["raw_json": 1]
    }
    
    /*
     override if needed
     put defaultParameters in new parameters that needed for request
    */
    var task: Moya.Task {
        return .requestParameters(parameters: defaultParameters, encoding: parameterEncoding)
    }
    
    var parameterEncoding: Moya.ParameterEncoding {
        return method == .get ? URLEncoding() : JSONEncoding()
    }
    
    var sampleData: Data {
        return Data()
    }
}
