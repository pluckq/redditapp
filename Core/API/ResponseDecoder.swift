//
//  DateDecoder.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 16/08/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Foundation

struct ResponseDecoder {
    
    private static var jsonDecoder: JSONDecoder {
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        jsonDecoder.dateDecodingStrategy = .secondsSince1970
        return jsonDecoder
    }
    
    static func decode<T: Codable>(_ data: Data) throws -> T {
        return try jsonDecoder.decode(T.self, from: data)
    }
}
