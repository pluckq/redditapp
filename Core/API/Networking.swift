//
//  Networking.swift
//  RedditApp
//
//  Created by vadim vitvickiy on 15/08/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Future
import Moya

struct RequestProvider {
    
    static func execute<T: NetworkingProtocol>(request: T) -> Future<T.MappingModel, Error> {
        
//        let logger = NetworkLoggerPlugin(verbose: true, cURL: true, output: nil, requestDataFormatter: nil, responseDataFormatter: nil)
        
        let provider = MoyaProvider<T>(endpointClosure: { target -> Endpoint in
            return Endpoint(url: request.url,
                            sampleResponseClosure: {.networkResponse(200, request.sampleData)},
                            method: request.method,
                            task: request.task,
                            httpHeaderFields: request.headers)}, plugins: [])
        
        return Future<T.MappingModel, Error> { promise in
            provider.request(request,
                             callbackQueue: DispatchQueue.global(qos: .background),
                             completion: { response in
                                switch response {
                                case .success(let result):
                                    if result.statusCode < 300 {
                                        do {
                                            let responseModel: T.MappingModel = try ResponseDecoder.decode(result.data)
                                            promise.succeed(value: responseModel)
                                        } catch {
                                            print(error)
                                            promise.fail(error: ServerError.mappingFailure)
                                        }
                                    } else {
                                        promise.fail(error: ServerError.serverError(code: result.statusCode))
                                    }
                                case .failure(let error):
                                    promise.fail(error: ServerError.unknown(message: error.localizedDescription))
                                }
            })
        }
    }
}



