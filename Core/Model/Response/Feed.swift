import Foundation

struct FeedRoot: Codable {
    let kind: String?
    let data: FeedInfo?
}

struct FeedInfo: Codable {
    let modhash: String?
    let dist: Int?
    let children: [Feed]?
    let after: String?
    let before: String?
}

struct Feed: Codable {
    let kind: String?
    let data: FeedData?
}

struct FeedData: Codable {
    
    enum FeedType {
        case text
        case image(preview: URL?, image: URL?)
        case video(preview: URL?, video: URL?)
        case gif(preview: URL?, gif: URL?)
        case link
    }
    
    let id: String?
    let name: String?
    let url: String?
    let title: String?
    let selftext: String?
    let selftextHTML: String?
    let author: String?
    let authorFullname: String?
    let postHint: PostHint?
    let clicked: Bool?
    let visited: Bool?
    let hidden: Bool?
    
    let subredditNamePrefixed: String?
    let subreddit: String?
    let subredditType: String?
    let subredditID: String?
    
    let permalink: String?
    
    let category: String?
    let over18: Bool?
    let canGild, spoiler, locked: Bool?
    
    let created: Date?
    let createdUtc: Date?
    
    let ups: Int?
    let downs: Int?
    let score: Int?
    let likes: Int?
    let numCrossposts: Int?
    let numComments: Int?
    
    let mediaEmbed: MediaEmbed?
    let secureMedia: Media?
    let secureMediaEmbed: MediaEmbed?
    let media: Media?
    let isVideo: Bool?
    
    let thumbnail: String?
    let thumbnailHeight: Int?
    let thumbnailWidth: Int?
    
    let preview: Preview?
    let mediaOnly: Bool?
    
    let crosspostParentList: [Feed]?
    let crosspostParent: String?
    
    func type() -> FeedType {
        if let postHint = postHint {
            switch postHint {
            case .image:
                return .image(preview: nil, image: nil)
            case .video, .hostedVideo, .richVideo:
                return .video(preview: nil, video: nil)
            case .link:
                return .link
            case .postHintSelf:
                return .text
            }
        }
        return .text
    }
}

struct Media: Codable {
    let oembed: Oembed?
    let type: String?
}

struct Oembed: Codable {
    let providerUrl, thumbnailUrl: URL?
    let description, title, type, html, version, providerName, authorName, authorUrl: String?
    let thumbnailWidth, thumbnailHeight, height, width: Int?
}

struct MediaEmbed: Codable {
    let content: String?
    let width: Int?
    let scrolling: Bool?
    let height: Int?
    let mediaDomainUrl: String?
}

struct Preview: Codable {
    let images: [Image]?
    let enabled: Bool?
    let redditVideoPreview: RedditVideoPreview?
}

struct Image: Codable {
    let source: Source?
    let resolutions: [Source]?
    let variants: Variants?
    let id: String?
}

struct Source: Codable {
    let url: String?
    let width, height: Int?
}

struct Variants: Codable {
    let obfuscated, nsfw, gif, mp4: ImageMedia?
}

struct ImageMedia: Codable {
    let source: Source?
    let resolutions: [Source]?
}

struct RedditVideoPreview: Codable {
    let fallbackUrl, scrubberMediaUrl, dashUrl, hlsUrl: URL?
    let height, width, duration: Int?
    let isGif: Bool?
    let transcodingStatus: String?
}

enum PostHint: String, Codable {
    case image = "image"
    case video = "video"
    case link = "link"
    case postHintSelf = "self"
    case richVideo = "rich:video"
    case hostedVideo = "hosted:video"
}
