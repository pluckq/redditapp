//
//  SubredditFetcher.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 26/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Foundation
import GRDB

protocol SubredditFetcherHandler {
    init(nextHandler: SubredditFetcherHandler?)
    func getSubreddit(_ subredditID: String, onCompleted: ((Subreddit?) -> ())?)
}

final class SubredditFetcher: SubredditFetcherHandler {
    
    private let nextHandler: SubredditFetcherHandler?
    private var cachedSubreddits = [String: Subreddit]()
    
    required init(nextHandler: SubredditFetcherHandler?) {
        self.nextHandler = nextHandler
    }
    
    func getSubreddit(_ subredditID: String, onCompleted: ((Subreddit?) -> ())?) {
        
        if let cached = cachedSubreddits[subredditID] {
            onCompleted?(cached)
        } else {
            nextHandler?.getSubreddit(subredditID, onCompleted: { [weak self] subreddit in
                if let subreddit = subreddit {
                    self?.cachedSubreddits[subredditID] = subreddit
                }
                onCompleted?(subreddit)
            })
        }
    }
}

final class SubredditFetcherBD: SubredditFetcherHandler {
    
    private let nextHandler: SubredditFetcherHandler?
    
    required init(nextHandler: SubredditFetcherHandler?) {
        self.nextHandler = nextHandler
    }
    
    func getSubreddit(_ subredditID: String, onCompleted: ((Subreddit?) -> ())?) {
        
    }
}

final class SubredditFetcherNetwork: SubredditFetcherHandler {
    
    private let nextHandler: SubredditFetcherHandler?
    
    required init(nextHandler: SubredditFetcherHandler?) {
        self.nextHandler = nextHandler
    }
    
    func getSubreddit(_ subredditID: String, onCompleted: ((Subreddit?) -> ())?) {
        
    }
}

final class SubredditFetcherEndOfChain: SubredditFetcherHandler {
    
    required init(nextHandler: SubredditFetcherHandler?) {
        
    }
    
    func getSubreddit(_ subredditID: String, onCompleted: ((Subreddit?) -> ())?) {
        print("If you're reading this, it't the end of the road my friend :)")
        onCompleted?(nil)
    }
}
