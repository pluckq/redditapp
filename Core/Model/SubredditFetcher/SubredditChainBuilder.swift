//
//  SubredditChainBuilder.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 26/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Foundation

final class SubredditChainBuilder {

    static func mainChain() -> SubredditFetcherHandler {
        let endOfChain = SubredditFetcherEndOfChain(nextHandler: nil)
        let network = SubredditFetcherNetwork(nextHandler: endOfChain)
        let bd = SubredditFetcherBD(nextHandler: network)
        let memory = SubredditFetcher(nextHandler: bd)
        
        return memory
    }
}
