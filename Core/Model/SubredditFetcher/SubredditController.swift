//
//  SubredditController.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 26/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Foundation

final class SubredditController {

    private let subredditHandler: SubredditFetcherHandler
    
    init(subredditHandler: SubredditFetcherHandler) {
        self.subredditHandler = subredditHandler
    }
    
    func getSubreddit(_ subredditID: String, onCompleted: ((Subreddit?) -> ())?) {
        subredditHandler.getSubreddit(subredditID, onCompleted: onCompleted)
    }
}
