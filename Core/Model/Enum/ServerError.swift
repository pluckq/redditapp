//
//  ServerError.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 09/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Foundation

enum ServerError: Error {
    case mappingFailure
    case serverError(code: Int)
    case listEmpty
    case mesage(message: String)
    case unknown(message: String)
}
