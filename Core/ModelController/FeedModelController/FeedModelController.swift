//
//  FeedModelController.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 02/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Foundation
import Moya
import Future

final class FeedPlainModelController: FeedModelControllerProtocol, Bindable {
    
    weak var delegate: FeedModelControllerDelegate?
    
    var shouldFetchMore: Bool {
        return next != nil && isLoading == false
    }
    
    var type: FeedType = .best {
        didSet {
            next = nil
            loadFeed()
        }
    }
    
    private var isLoading = false
    private var limit: Int = 30
    private var next: String?
    
    func loadFeed(_ context: ASBatchContext? = nil) {
        guard isLoading == false else { return }
        
        isLoading = true
        
        RequestProvider
            .execute(request: FeedRequest(type: type, limit: limit, next: next))
            .observe(on: DispatchQueue.global(qos: .userInitiated))
            .on(success: weak(weakSelf.didLoadData),
                failure: weak(weakSelf.didFailLoadData),
                completion: { [weak self, weak context] _ in
                    self?.isLoading = false
                    context?.completeBatchFetching(true)
            })
    }
    
    private func didLoadData(response: FeedRoot) {
        next = response.data?.after
        if let feedItems = response.data?.children, feedItems.count > 0 {
            delegate?.didUpdateData(feedItems)
        }
    }
    
    private func didFailLoadData(error: Error) {
        if let serverError = error as? ServerError {
            delegate?.didFailToUpdateData(error: serverError)
        }
    }
}
