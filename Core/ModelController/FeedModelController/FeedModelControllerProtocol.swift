//
//  FeedModelControllerProtocol.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 07/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Foundation

protocol FeedModelControllerProtocol: class {
    var delegate: FeedModelControllerDelegate? { get set }
    var shouldFetchMore: Bool { get }
    var type: FeedType { get set }
    
    func loadFeed(_ context: ASBatchContext?)
}

extension FeedModelControllerProtocol {
    func loadFeed(_ context: ASBatchContext? = nil) {
        return loadFeed(context)
    }
}
