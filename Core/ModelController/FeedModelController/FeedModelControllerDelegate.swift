//
//  FeedModelControllerDelegate.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 07/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Foundation

protocol FeedModelControllerDelegate: class {
    //Note: methods called on background thread
    func didUpdateData(_ data: [Feed])
    func didFailToUpdateData(error: ServerError)
}
