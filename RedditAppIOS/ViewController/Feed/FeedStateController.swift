//
//  FeedStateController.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 21/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Foundation
import GameplayKit

final class FeedWaitingState: GKState {
    
    override func isValidNextState(_ stateClass: AnyClass) -> Bool {
        return stateClass == FeedRecievedState.self
            || stateClass == FeedEmptyState.self
            || stateClass == FeedErrorState.self
    }
    
    override func didEnter(from previousState: GKState?) {
        print("wating data start")
    }
    
    override func willExit(to nextState: GKState) {
        print("end of wait")
    }
}

final class FeedEmptyState: GKState {
    
    override func isValidNextState(_ stateClass: AnyClass) -> Bool {
        return stateClass == FeedWaitingState.self
    }
    
    override func didEnter(from previousState: GKState?) {
        print("data not awaiable start")
    }
    
    override func willExit(to nextState: GKState) {
        print("data not awaiable end")
    }
}

final class FeedRecievedState: GKState {
    
    override func isValidNextState(_ stateClass: AnyClass) -> Bool {
        return stateClass == FeedWaitingState.self
    }
    
    override func didEnter(from previousState: GKState?) {
        print("data awaiable start")
    }
    
    override func willExit(to nextState: GKState) {
        print("data awaiable end")
    }
}

final class FeedErrorState: GKState {
    
    override func isValidNextState(_ stateClass: AnyClass) -> Bool {
        return stateClass == FeedWaitingState.self
    }
    
    override func didEnter(from previousState: GKState?) {
        print("error state start")
    }
    
    override func willExit(to nextState: GKState) {
        print("error state end")
    }
}

final class FeedStateController: GKStateMachine {

    
}
