//
//  FeedViewController.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 18/08/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

final class FeedViewController: ASViewController<ASCollectionNode>, DataPresentable {
    
    typealias Dependencies = HasFeedModelController & HasFeedDataSource
    
    private let dependencies: Dependencies
    private let collectionNode: ASCollectionNode
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
        let flowLayout = UICollectionViewFlowLayout()
        collectionNode = ASCollectionNode(collectionViewLayout: flowLayout)
        
        super.init(node: collectionNode)
        
        flowLayout.minimumLineSpacing = 1
        flowLayout.minimumInteritemSpacing = 1
        
        collectionNode.delegate = self
        collectionNode.dataSource = dependencies.dataSource
        
        dependencies.modelController.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Feed"
        
        collectionNode.view.delaysContentTouches = false
        
        dependencies.modelController.loadFeed()
    }
}

extension FeedViewController: ASCollectionDelegate {
    
    func shouldBatchFetch(for collectionNode: ASCollectionNode) -> Bool {
        return dependencies.modelController.shouldFetchMore
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, willBeginBatchFetchWith context: ASBatchContext) {
        dependencies.modelController.loadFeed(context)
    }
}

extension FeedViewController: FeedModelControllerDelegate {
    
    func didUpdateData(_ data: [Feed]) {
        dependencies.dataSource.updateItems?(data)
        
        let cellsCount = collectionNode.numberOfItems(inSection: 0)
        let itemsCount = data.count - 1
        var indexPaths = [IndexPath]()
        for i in cellsCount...cellsCount + itemsCount {
            indexPaths.append(IndexPath(item: i, section: 0))
        }
        
        DispatchQueue.main.async { [weak collectionNode] in
            collectionNode?.performBatchUpdates({
                collectionNode?.insertItems(at: indexPaths)
            }, completion: { _ in })
        }
    }
    
    func didFailToUpdateData(error: ServerError) {
        showError(error)
    }
}
