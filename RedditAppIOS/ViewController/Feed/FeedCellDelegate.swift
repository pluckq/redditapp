//
//  FeedCellController.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 07/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

protocol FeedCellDelegateProtocol: class {
    //Outputs
    var options: Observable<FeedData> { get set }
    var upvote: Observable<String> { get set }
    var downvote: Observable<String> { get set }
    var showAuthor: Observable<String> { get set }
    var showSubreddit: Observable<String> { get set }
}


final class FeedCellDelegate: FeedCellDelegateProtocol {
    
    var options: Observable<FeedData>
    var upvote: Observable<String>
    var downvote: Observable<String>
    var showAuthor: Observable<String>
    var showSubreddit: Observable<String>
    
    private let feed: FeedData? = nil
}
