//
//  FeedCellController.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 07/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

protocol FeedCellControllerProtocol: class {
    //Outputs
    var options: Observable<FeedData> { get set }
    var upvote: Observable<String> { get set }
    var downvote: Observable<String> { get set }
    var showAuthor: Observable<String> { get set }
    var showSubreddit: Observable<String> { get set }
}


class FeedCellDelegate: ASViewController<ASCellNode>, FeedCellControllerProtocol {
    
    var options: Observable<FeedData>
    var upvote: Observable<String>
    var downvote: Observable<String>
    var showAuthor: Observable<String>
    var showSubreddit: Observable<String>
    
    private let feed: FeedData?
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(node: ASCellNode, feed: FeedData?) {
        self.feed = feed
        
        super.init(node: node)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("qwe")
    }
}
