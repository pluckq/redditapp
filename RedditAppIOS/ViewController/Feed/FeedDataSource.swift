//
//  FeedDataSource.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 03/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

protocol FeedDataSource: class, ASCollectionDataSource {
    //Inputs:
    var updateItems: Observable<[Feed]> { get set }
}

final class FeedPlainDataSource: NSObject, FeedDataSource {
    
    var updateItems: Observable<[Feed]>
    
    private let feedCellFabric: FeedCellFabricProtocol = FeedCellFabric()
    private var items: [Feed] = []

    override init() {
        super.init()
        
        updateItems = weak(weakSelf.addItems)
    }
    
    private func addItems(items: [Feed]) {
        self.items.append(contentsOf: items)
    }
    
    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        return 1
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let feedModel = items[indexPath.item].data
        return { [unowned feedCellFabric] in
            return feedCellFabric.createCellWith(feed: feedModel)
        }
    }
}
