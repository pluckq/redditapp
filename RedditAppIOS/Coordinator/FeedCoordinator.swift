//
//  FeedCoordinator.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 18/08/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

final class FeedCoordinator: Coordinator {
    
    typealias Dependencies = HasTabbarRouter & HasFeedViewControllersFabric
    
    var childCoordinators: [Coordinator] = []
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    func start() {
        let feedModule = dependencies.feedViewControllersFabric.createFeedViewControllerModule()
        dependencies.tabbarRouter.appendViewController(feedModule, animated: false)
    }
}
