//
//  ApplicationCoordinator.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 16/08/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

final class ApplicationCoordinator: Coordinator {
    
    typealias Dependencies = HasWindowRouter & HasTabbarRouter

    var childCoordinators: [Coordinator] = []
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    func start() {
        let mainTabbarCoordinator = MainTabbarCoordinator(dependencies: TabbarCoordinatorDependencies(tabbarRouter: dependencies.tabbarRouter))
        addDependency(mainTabbarCoordinator)
        
        dependencies.windowRouter.setRootModule(dependencies.tabbarRouter)
    }
}
