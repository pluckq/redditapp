//
//  SubredditsCoordinator.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 18/08/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

final class SubredditsCoordinator: Coordinator {
    
    typealias Dependencies = HasTabbarRouter
    
    var childCoordinators: [Coordinator] = []
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    func start() {
        let subredditsVC = SubredditsViewController()
        let subredditsNC = FSNavigationController(rootViewController: subredditsVC)
        subredditsNC.tabBarItem = UITabBarItem(title: "Subreddits", image: nil, tag: 1)
        dependencies.tabbarRouter.appendViewController(subredditsNC, animated: false)
    }
}
