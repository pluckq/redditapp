//
//  MainTabbarCoordinator.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 18/08/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

final class MainTabbarCoordinator: Coordinator, Bindable {
    
    typealias Dependencies = HasTabbarRouter
    
    var childCoordinators: [Coordinator] = []
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func start() {
        let feedViewControllersFabric = FeedViewControllersFabric()
        let feedCoordinator = FeedCoordinator(dependencies: ProfileCoordinatorDependencies(tabbarRouter: dependencies.tabbarRouter, feedViewControllersFabric: feedViewControllersFabric))
        addDependency(feedCoordinator)
        
        let subredditsCoordinator = SubredditsCoordinator(dependencies: TabbarCoordinatorDependencies(tabbarRouter: dependencies.tabbarRouter))
        addDependency(subredditsCoordinator)
        
        let profileCoordinator = ProfileCoordinator(dependencies: TabbarCoordinatorDependencies(tabbarRouter: dependencies.tabbarRouter))
        addDependency(profileCoordinator)
    }
}
