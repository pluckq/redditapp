//
//  ProfileCoordinator.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 18/08/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

final class ProfileCoordinator: Coordinator {
    
    typealias Dependencies = HasTabbarRouter
    
    var childCoordinators: [Coordinator] = []
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    func start() {
        let profileViewController = ProfileViewController()
        let profileNavigationController = FSNavigationController(rootViewController: profileViewController)
        profileViewController.tabBarItem = UITabBarItem(title: "Profile", image: nil, tag: 2)
        dependencies.tabbarRouter.appendViewController(profileNavigationController, animated: false)
    }
}
