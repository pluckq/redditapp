//
//  AppDelegate.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 14/08/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    lazy var mainTabbarVC = MainTabBarViewController()
    lazy var redditOauth: RedditOauth = RedditOauth()
    
    lazy var applicationCoordinator: ApplicationCoordinator = {
        let windowRouter = WindowRouter(window: window)
        let tabbarRouter = TabbarRouter(tabbarVC: mainTabbarVC)
        let dependencies = ApplicationCoordinatorDependencies(windowRouter: windowRouter, tabbarRouter: tabbarRouter)
        
        return ApplicationCoordinator(dependencies: dependencies)
    }()
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        ThemeManager.shared.theme = DefaultTheme()
        
        return true
    }


    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        applicationCoordinator.start()
        
        return true
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        redditOauth.registerAuthWith(url: url)
        //auth example
//        NSURL *authorizationURL = [NSURL URLWithString:@"https://ssl.reddit.com/api/v1/authorize?client_id=Bq9yaGfIVdJXTQ&response_type=code&state=TEST&redirect_uri=myappscheme://response&duration=permanent&scope=read"];
//        NSURLRequest *request = [NSURLRequest requestWithURL:authorizationURL];
//        [self.webView loadRequest:request];
        return true
    }
}

