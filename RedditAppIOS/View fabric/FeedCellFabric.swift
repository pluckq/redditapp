//
//  FeedCellFabric.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 06/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

protocol FeedCellFabricProtocol: class {
    func createCellWith(feed: FeedData?) -> ASCellNode
}

final class FeedCellFabric: FeedCellFabricProtocol {
    
    private let feedViewModelFabric: FeedViewModelFabricProtocol = FeedViewModelFabric()

    func createCellWith(feed: FeedData?) -> ASCellNode {
        return textCell(feed)
    }
    
    private func textCell(_ feed: FeedData?) -> FeedTextNode {
        let textNodeDependencies = feedViewModelFabric.createTextCellDependencies(feed: feed)
        let cell = FeedTextNode(dependencies: textNodeDependencies)
        return cell
    }
}
