//
//  FeedViewModelFabric.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 13/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Foundation
import DateToolsSwift

protocol FeedViewModelFabricProtocol {
    func createTextCellDependencies(feed: FeedData?) -> FeedTextNode.TextCellDependencies
}

final class FeedViewModelFabric: FeedViewModelFabricProtocol {
    
    func createTextCellDependencies(feed: FeedData?) -> FeedTextNode.TextCellDependencies {
        let topViewModel = FeedTopViewModel(date: feed?.createdUtc?.timeAgoSinceNow)
        let bottomViewModel = FeedBottomViewModel(author: feed?.author, subreddit: feed?.subreddit, upvotes: feed?.ups.string(), commentsCount: feed?.numComments.string())
        let textViewModel = FeedTextViewModel(title: feed?.title, text: feed?.selftext?.trimmingCharacters(in: .whitespacesAndNewlines))
        
        return FeedTextNodeDependencies(textViewModel: textViewModel, topViewModel: topViewModel, bottomViewModel: bottomViewModel)
    }
}
