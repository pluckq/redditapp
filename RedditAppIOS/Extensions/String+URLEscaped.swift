//
//  String+URLEscaped.swift
//  Forms
//
//  Created by vadim vitvickiy on 18/04/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
}
