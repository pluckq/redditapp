//
//  String+AttributedString.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 06/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

extension String {
    func attributed() -> NSAttributedString {
        return NSAttributedString(string: self)
    }
}

extension Optional where Wrapped == String {
    func attributed() -> NSAttributedString? {
        if let string = self {
            return NSAttributedString(string: string)
        }
        return nil
    }
}
