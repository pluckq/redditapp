//
//  Codable+Dictionary.swift
//  Forms
//
//  Created by vadim vitvickiy on 18/04/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

extension Encodable {
    func asDictionary(_ dateFormat: String) throws -> [String: Any] {
        let encoder = JSONEncoder()
        encoder.keyEncodingStrategy = .convertToSnakeCase
        encoder.dateEncodingStrategy = .custom({ (date, encoder) in
            let formatter = DateFormatter()
            formatter.dateFormat = dateFormat
            let stringData = formatter.string(from: date)
            var container = encoder.singleValueContainer()
            try container.encode(stringData)
        })
        let data = try encoder.encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw NSError()
        }
        return dictionary
    }
}
