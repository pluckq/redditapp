//
//  UIButtonExtension.swift
//  Getpass
//
//  Created by vadim vitvickiy on 10.02.17.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit

fileprivate let minimumHitArea = CGSize(width: 50, height: 40)

extension UIButton {
    open override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if self.isHidden || !self.isUserInteractionEnabled || self.alpha < 0.01 { return nil }
        let buttonSize = bounds.size
        let widthToAdd = max(minimumHitArea.width - buttonSize.width, 0)
        let heightToAdd = max(minimumHitArea.height - buttonSize.height, 0)
        let largerFrame = bounds.insetBy(dx: -widthToAdd / 2, dy: -heightToAdd / 2)
        return (largerFrame.contains(point)) ? self : nil
    }
}
