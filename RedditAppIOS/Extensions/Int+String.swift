//
//  Int+ String.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 16/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

extension Optional where Wrapped == Int {
    
    func string() -> String? {
        if let number = self {
            return String(number)
        }
        return nil
    }
}
