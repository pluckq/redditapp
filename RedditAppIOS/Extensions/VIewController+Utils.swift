//
//  VIewControllerExtension.swift
//  Getpass
//
//  Created by vadim vitvickiy on 19.12.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func add(_ child: UIViewController) {
        addChild(child)
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    
    func remove() {
        guard parent != nil else {
            return
        }
        willMove(toParent: nil)
        removeFromParent()
        view.removeFromSuperview()
    }
}
