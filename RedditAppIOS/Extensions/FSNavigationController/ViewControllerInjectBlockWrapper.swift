import UIKit

final class ViewControllerInjectBlockWrapper {
    
    var block: ((_ viewController: UIViewController, _ animated: Bool) -> Void)?
    
    init(block: ((_ viewController: UIViewController, _ animated: Bool) -> Void)?) {
        self.block = block
    }
}
