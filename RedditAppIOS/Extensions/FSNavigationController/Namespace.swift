import UIKit

protocol NamespaceWrappable: class {
    
    associatedtype WrapperType
    
    var fs: WrapperType { get set }
    
    static var fs: WrapperType.Type { get }
}

extension NamespaceWrappable {

    var fs: TypeWrapper<Self> {
        get {
            return TypeWrapper(self)
        }
        set { }
    }
    
    static var fs: TypeWrapper<Self>.Type {
        return TypeWrapper.self
    }
}

protocol TypeWrapperProtocol {
    
    associatedtype WrapperObject
    
    var wrapperObject: WrapperObject { get }
    
    init(_ wrapperObject: WrapperObject)
    
}

struct TypeWrapper<T>: TypeWrapperProtocol {
    
    public let wrapperObject: T
    
    public init(_ wrapperObject: T) {
        self.wrapperObject = wrapperObject
    }
}


