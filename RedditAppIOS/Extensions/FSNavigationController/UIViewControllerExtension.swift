import UIKit

extension UIViewController: NamespaceWrappable { }

private extension TypeWrapper {
    var viewController: T {
        return wrapperObject
    }
}

fileprivate struct AssociatedKey {
    static var viewWillAppearInjectBlock    = 0
    static var navigationBarHidden          = 0
    static var popGestrueEnable             = 0
    static var navigationBarBackgroundColor = 0
    static var popGestrueEnableWidth        = 0
    static var navigationBarTitleColor      = 0
    static var gestureDelegate              = 0
}

extension TypeWrapper where T: UIViewController {
    
    static func load() {
        UIViewController.methodsSwizzling
    }
    
    var navigationBarHidden: Bool {
        get {
            var hidden = objc_getAssociatedObject(self.viewController, &AssociatedKey.navigationBarHidden) as? NSNumber
            if hidden == nil {
                hidden = NSNumber(booleanLiteral: false)
                objc_setAssociatedObject(self.viewController,
                                         &AssociatedKey.navigationBarHidden,
                                         hidden,
                                         .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
            return hidden!.boolValue
        }
        set {
            objc_setAssociatedObject(self.viewController,
                                     &AssociatedKey.navigationBarHidden,
                                     NSNumber(booleanLiteral: newValue),
                                     .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    var popGestrueEnable: Bool {
        get {
            var enable = objc_getAssociatedObject(self.viewController, &AssociatedKey.popGestrueEnable) as? NSNumber
            if enable == nil {
                enable = NSNumber(booleanLiteral: true)
                objc_setAssociatedObject(self.viewController,
                                         &AssociatedKey.popGestrueEnable,
                                         enable,
                                         .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
            return enable!.boolValue
        }
        set {
            objc_setAssociatedObject(self.viewController,
                                     &AssociatedKey.popGestrueEnable,
                                     NSNumber(booleanLiteral: newValue),
                                     .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    var popGestrueEnableWidth: CGFloat {
        get {
            var width = objc_getAssociatedObject(self.viewController, &AssociatedKey.popGestrueEnableWidth) as? NSNumber
            if width == nil {
                width = NSNumber(floatLiteral: 0)
                objc_setAssociatedObject(self.viewController,
                                         &AssociatedKey.popGestrueEnableWidth,
                                         width,
                                         .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
            return CGFloat(width!.floatValue)
        }
        set {
            objc_setAssociatedObject(self.viewController,
                                     &AssociatedKey.popGestrueEnableWidth,
                                     NSNumber(floatLiteral: Double(newValue)),
                                     .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    var navigationBarBackgroundColor: UIColor? {
        get {
            return objc_getAssociatedObject(self.viewController, &AssociatedKey.navigationBarBackgroundColor) as? UIColor
        }
        set {
            objc_setAssociatedObject(self.viewController, &AssociatedKey.navigationBarBackgroundColor, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    var navigationBarTitleColor: UIColor? {
        get {
            return objc_getAssociatedObject(self.viewController, &AssociatedKey.navigationBarTitleColor) as? UIColor
        }
        set {
            objc_setAssociatedObject(self.viewController, &AssociatedKey.navigationBarTitleColor, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    weak var gestureDelegate: UIGestureRecognizerDelegate? {
        get {
            return gestureDelegateWrapper.instance
        }
        set {
            gestureDelegateWrapper.instance = newValue
        }
    }
    
    private var gestureDelegateWrapper: WeakReferenceWrapper<UIGestureRecognizerDelegate> {
        var wrapper = objc_getAssociatedObject(self.viewController, &AssociatedKey.gestureDelegate) as? WeakReferenceWrapper<UIGestureRecognizerDelegate>
        if wrapper == nil {
            wrapper = WeakReferenceWrapper<UIGestureRecognizerDelegate>()
            objc_setAssociatedObject(self.viewController, &AssociatedKey.gestureDelegate, wrapper, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        return wrapper!
    }
    
    var viewWillAppearInjectBlock: ViewControllerInjectBlockWrapper? {
        get {
            return objc_getAssociatedObject(self.viewController, &AssociatedKey.viewWillAppearInjectBlock) as? ViewControllerInjectBlockWrapper
        }
        set {
            objc_setAssociatedObject(self.viewController, &AssociatedKey.viewWillAppearInjectBlock, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}

extension UIViewController {
    
    fileprivate static let methodsSwizzling: () = {
        guard let originalViewWillAppearSelector = class_getInstanceMethod(UIViewController.self, #selector(viewWillAppear(_:))),
            let swizzledViewWillAppearSelector = class_getInstanceMethod(UIViewController.self, #selector(fs_viewWillAppear(_:))) else {
                return
        }
        method_exchangeImplementations(originalViewWillAppearSelector, swizzledViewWillAppearSelector)
    }()
    
    @objc fileprivate func fs_viewWillAppear(_ animated: Bool) {
        fs_viewWillAppear(animated)
        fs.viewWillAppearInjectBlock?.block?(self, animated)
    }
}
