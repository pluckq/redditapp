import UIKit

final class FSNavigationController: ASNavigationController {
    
    lazy public var fullscreenInteractivePopGestureRecognizer = UIPanGestureRecognizer()
    
    private lazy var popGestrueDelegate: PopGestrueDelegateObject = {
        let delegate = PopGestrueDelegateObject()
        delegate.navigationController = self
        return delegate
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initFullscreenGesture()
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        let blockWrapper = ViewControllerInjectBlockWrapper { (viewController: UIViewController, animated: Bool) -> Void in
            viewController.navigationController?
                .setNavigationBarHidden(viewController.fs.navigationBarHidden, animated: animated)
            if let navigationController = viewController.navigationController as? FSNavigationController {
                navigationController.popGestrueDelegate.delegate = viewController.fs.gestureDelegate
            }
        }
        viewController.fs.viewWillAppearInjectBlock = blockWrapper
        viewControllers.last?.fs.viewWillAppearInjectBlock = blockWrapper
        super.pushViewController(viewController, animated: animated)
    }
    
    private func initFullscreenGesture() {
        fullscreenInteractivePopGestureRecognizer.delegate = popGestrueDelegate
        guard let targets = interactivePopGestureRecognizer?.value(forKey: "targets") as? [AnyObject] else { return }
        guard let target = (targets.first as? NSObject)?.value(forKey: "target") else { return }
        let internalAction = NSSelectorFromString("handleNavigationTransition:")
        fullscreenInteractivePopGestureRecognizer.maximumNumberOfTouches = 1
        fullscreenInteractivePopGestureRecognizer.addTarget(target, action: internalAction)
        interactivePopGestureRecognizer?.isEnabled = false
        interactivePopGestureRecognizer?.view?.addGestureRecognizer(fullscreenInteractivePopGestureRecognizer)
    }
}
