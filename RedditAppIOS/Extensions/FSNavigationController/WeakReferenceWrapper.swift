final class WeakReferenceWrapper<WrappedType: AnyObject> {
    weak var instance: WrappedType?
}
