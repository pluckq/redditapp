//
//  Date+String.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 13/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

extension Date {
    
    enum DateType: String {
        case short = "MM.dd.yyyy"
        case long = "MMMM dddd yyyy"
    }
    
    func string(_ type: DateType) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = type.rawValue
        return dateFormatter.string(from: self)
    }
}
