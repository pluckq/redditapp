//
//  String+DateFormats.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 30/10/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit

extension String {
    struct DateFormats {
        static let full = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        static let withoutMS = "yyyy-MM-dd'T'HH:mm:ssZ"
        static let date = "yyyy-MM-dd"
        static let appDateFormat = "dd.MM.yyyy"
        static let appTimeFormat = "HH:mm"
    }
    
    static func timeInterval(_ time: TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format: "%02i:%02i.%02i", hours, minutes, seconds)
    }
}
