//
//  ASControlNode+Action.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 09/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

typealias ASControlNodeTargetClosure = (ASControlNode) -> ()

private class ASControlNodeClosureWrapper: NSObject {
    let closure: ASControlNodeTargetClosure
    init(_ closure: @escaping ASControlNodeTargetClosure) {
        self.closure = closure
    }
}

extension ASControlNode {
    
    private struct AssociatedKeys {
        static var targetClosure = "targetClosure"
    }
    
    private var targetClosure: ASControlNodeTargetClosure? {
        get {
            guard let closureWrapper = objc_getAssociatedObject(self, &AssociatedKeys.targetClosure) as? ASControlNodeClosureWrapper else { return nil }
            return closureWrapper.closure
        }
        set(newValue) {
            guard let newValue = newValue else { return }
            objc_setAssociatedObject(self, &AssociatedKeys.targetClosure, ASControlNodeClosureWrapper(newValue), objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func tap(closure: @escaping ASControlNodeTargetClosure) {
        targetClosure = closure
        
        addTarget(self, action: #selector(ASControlNode.closureAction), forControlEvents: .touchUpInside)
    }
    
    @objc final private func closureAction() {
        guard let targetClosure = targetClosure else { return }
        targetClosure(self)
    }
}
