//
//  UIButton+Action.swift
//  Forms
//
//  Created by vadim vitvickiy on 10/08/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

typealias UIControlTargetClosure = (UIControl) -> ()

private class ClosureWrapper: NSObject {
    let closure: UIControlTargetClosure
    init(_ closure: @escaping UIControlTargetClosure) {
        self.closure = closure
    }
}

extension UIControl {
    
    private struct AssociatedKeys {
        static var targetClosure = "targetClosure"
    }
    
    private var targetClosure: UIControlTargetClosure? {
        get {
            guard let closureWrapper = objc_getAssociatedObject(self, &AssociatedKeys.targetClosure) as? ClosureWrapper else { return nil }
            return closureWrapper.closure
        }
        set(newValue) {
            guard let newValue = newValue else { return }
            objc_setAssociatedObject(self, &AssociatedKeys.targetClosure, ClosureWrapper(newValue), objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func tap(closure: @escaping UIControlTargetClosure) {
        targetClosure = closure
        
        addTarget(self, action: #selector(UIControl.closureAction), for: .touchUpInside)
    }
    
    @objc final private func closureAction() {
        guard let targetClosure = targetClosure else { return }
        targetClosure(self)
    }
}
