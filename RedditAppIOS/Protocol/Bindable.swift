//
//  Bindable.swift
//  Forms
//
//  Created by vadim vitvickiy on 24/08/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

/*
 Example:
 self.model.valueChanged = weak(Function.updateValue(to:))
 */
protocol Bindable: class {}

extension NSObject: Bindable { }

extension Bindable {
    
    typealias weakSelf = Self
    
    func weak<Args>(_ method: @escaping ((weakSelf) -> ((Args) -> Void))) -> ((Args) -> Void) {
        return { [weak self] arg in
            guard let self = self else { return }
            method(self)(arg)
        }
    }
    
    //implementation for empty args functions
    func weak(_ method: @escaping ((weakSelf) -> (() -> Void))) -> (() -> Void) {
        return { [weak self] in
            guard let self = self else { return }
            method(self)()
        }
    }
}

