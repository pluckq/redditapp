//
//  FeedNodeProtocol.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 09/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

protocol FeedNodeProtocol: class {
    
    associatedtype ContentView: ASDisplayNode
    
    var topView: FeedTopView { get }
    var bottomView: FeedBottomView { get }
    var contentView: ContentView { get }
}
