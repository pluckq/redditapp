//
//  NavigationInjectionProtocols.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 16/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Foundation

protocol HasWindowRouter {
    var windowRouter: WindowRouter { get }
}

protocol HasNavigationRouter {
    var router: NavigationRouter { get }
}

protocol HasTabbarRouter {
    var tabbarRouter: TabbarRouter { get }
}
