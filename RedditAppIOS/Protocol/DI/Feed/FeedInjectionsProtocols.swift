//
//  FeedInjectionsProtocols.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 16/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Foundation

//MARK: Model

protocol HasFeedModelController {
    var modelController: FeedModelControllerProtocol { get }
}

protocol HasFeedDataSource {
    var dataSource: FeedDataSource { get }
}

protocol HasFeedViewControllersFabric {
    var feedViewControllersFabric: FeedViewControllersFabricProtocol { get }
}

//MARK: View model

protocol HasFeedCellTopViewModel {
    var topViewModel: FeedTopViewModel { get }
}

protocol HasFeedCellBottomViewModel {
    var bottomViewModel: FeedBottomViewModel { get }
}

protocol HasFeedCellTextViewModel {
    var textViewModel: FeedTextViewModel { get }
}

protocol HasFeedCellImageViewModel {
    var imageViewModel: FeedImageViewModel { get }
}

protocol HasFeedCellVideoViewModel {
    var videoViewModel: FeedVideoViewModel { get }
}
