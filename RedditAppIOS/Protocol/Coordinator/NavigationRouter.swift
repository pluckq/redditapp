//
//  NavigationRouter.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 18/08/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

protocol NavigationRouterProtocol: Presentable {
    
    func push(_ module: Presentable?)
    
    func popModule(animated: Bool)
    
    func popToRootModule()
    
    func present(_ module: Presentable?)
    
    func dismissModule()
    
    func setRootModule(_ module: Presentable?, animated: Bool)
    
    func setControllers(_ controllers: [UIViewController])
    
    func clearStack()
}

final class NavigationRouter: NavigationRouterProtocol {
    
    private weak var rootController: UINavigationController?
    
    init(rootController: UINavigationController) {
        self.rootController = rootController
    }
    
    func toPresent() -> UIViewController? {
        return rootController
    }
    
    func present(_ module: Presentable?) {
        guard let controller = module?.toPresent() else { return }
        rootController?.present(controller, animated: true, completion: nil)
    }
    
    func dismissModule() {
        rootController?.dismiss(animated: true, completion: nil)
    }
    
    func push(_ module: Presentable?)  {
        guard let controller = module?.toPresent(), (controller is UINavigationController == false)
            else { assertionFailure("Deprecated push UINavigationController."); return }
        rootController?.pushViewController(controller, animated: true)
    }
    
    func popModule(animated: Bool)  {
        rootController?.popViewController(animated: animated)
    }
    
    func setRootModule(_ module: Presentable?, animated: Bool) {
        guard let controller = module?.toPresent() else { return }
        rootController?.setViewControllers([controller], animated: animated)
    }
    
    func popToRootModule() {
        rootController?.popToRootViewController(animated: true)
    }
    
    func setControllers(_ controllers: [UIViewController]) {
        rootController?.setViewControllers(controllers, animated: true)
    }
    
    func clearStack() {
        rootController?.setViewControllers([], animated: false)
    }
}
