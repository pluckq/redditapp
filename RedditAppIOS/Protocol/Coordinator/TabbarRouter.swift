//
//  TabbarRouter.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 18/08/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

protocol TabbarRouterProtocol: Presentable {
    func appendViewController(_ viewController: UIViewController, animated: Bool)
    func removeViewController(_ viewcontroller: UIViewController, animated: Bool)
    func setViewControllers(_ viewControllers: [UIViewController], animated: Bool)
}

final class TabbarRouter: TabbarRouterProtocol {
    
    private let tabbarVC: UITabBarController
    
    init(tabbarVC: UITabBarController) {
        self.tabbarVC = tabbarVC
    }
    
    func toPresent() -> UIViewController? {
        return tabbarVC
    }
    
    func appendViewController(_ viewController: UIViewController, animated: Bool) {
        var vcs: [UIViewController]
        
        if let tabbarStack = tabbarVC.viewControllers {
            vcs = tabbarStack
        } else {
            vcs = []
        }
        
        vcs.append(viewController)
        
        setViewControllers(vcs, animated: animated)
    }
    
    func removeViewController(_ viewcontroller: UIViewController, animated: Bool) {
        guard var vcs = tabbarVC.viewControllers else { return }
        
        for (index, vc) in vcs.enumerated() {
            if vc === viewcontroller {
                vcs.remove(at: index)
            }
        }
        
        setViewControllers(vcs, animated: animated)
    }
    
    func setViewControllers(_ viewControllers: [UIViewController], animated: Bool) {
        tabbarVC.setViewControllers(viewControllers, animated: animated)
    }
}
