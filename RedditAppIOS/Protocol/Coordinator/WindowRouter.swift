
//
//  WindowRouter.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 18/08/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

protocol WindowRouterProtocol {
    func setRootModule(_ module: Presentable?)
}

final class WindowRouter: WindowRouterProtocol {
    
    private weak var rootController: UIWindow?
    
    init(window: UIWindow?) {
        self.rootController = window
    }
    
    func setRootModule(_ module: Presentable?) {
        guard let controller = module?.toPresent() else { return }
        rootController?.rootViewController = controller
    }
}
