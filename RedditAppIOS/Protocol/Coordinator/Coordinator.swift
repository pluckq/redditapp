//
//  BaseCoordinator.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 16/08/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

protocol Coordinator: class {
    var childCoordinators: [Coordinator] { get set }
    func start()
}

extension Coordinator {
    func addDependency(_ coordinator: Coordinator) {
        if childCoordinators.contains(where: { $0 === coordinator }) { return }
        childCoordinators.append(coordinator)
        coordinator.start()
    }
    
    func removeDependency(_ coordinator: Coordinator?) {
        guard childCoordinators.isEmpty == false, let coordinator = coordinator else { return }
        for (index, element) in childCoordinators.enumerated() {
            if element === coordinator {
                childCoordinators.remove(at: index)
                break
            }
        }
    }
    
    func removeAllDependencies() {
        childCoordinators.removeAll()
    }
}
