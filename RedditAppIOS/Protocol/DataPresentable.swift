//
//  DataPresentable.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 09/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

protocol DataPresentable: class {
    func showError(_ error: ServerError)
}

extension DataPresentable where Self: UIViewController {
    
    func showError(_ error: ServerError) {
//        switch error {
//        case .mappingFailure:
//            break
//        case .serverError(let code):
//            break
//        case .listEmpty:
//            break
//        case .mesage(let message):
//            break
//        case .unknown(let message):
//            break
//        }
    }
    
    func showNoData() {
        
    }
}
