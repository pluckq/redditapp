//
//  Measures.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 05/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

protocol Measure {
    
}

struct IPhoneMeasure: Measure {
    
}

struct IPadMesure: Measure {
    
}

struct Measures {
    static let spacing = 15
    static let insets = UIEdgeInsets(top: 15, left: 15, bottom: 0, right: 15)
}
