//
//  Themable.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 10/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

protocol Themeable: class {
    func applyTheme(theme: Theme)
}
