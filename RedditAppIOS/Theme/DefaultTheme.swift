//
//  DefaultTheme.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 10/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit
import SwiftRichString

struct DefaultTheme: Theme {
    
    var backgroundColor: UIColor = UIColor(red: 243, green: 247, blue: 249, alpha: 1.0)
    
    var tintColor: UIColor = UIColor(red: 243, green: 247, blue: 249, alpha: 1.0)
    
    var navigationBarTintColor: UIColor? = UIColor(red: 243, green: 247, blue: 249, alpha: 1.0)
    
    var navigationBarTranslucent: Bool = true
    
    var navigationTitleFont: UIFont = .systemFont(ofSize: 17)
    
    var navigationTitleColor: UIColor = .black
    
    var headlineFont: UIFont = .systemFont(ofSize: 17, weight: .bold)
    
    var headlineColor: UIColor = .black
    
    var bodyTextFont: UIFont = .systemFont(ofSize: 15)
    
    var bodyTextColor: UIColor = .black
    
    var smallTextFont: UIFont = .systemFont(ofSize: 12, weight: .thin)
    
    var smallTextColor: UIColor = .lightGray
}
