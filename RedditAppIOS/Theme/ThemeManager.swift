//
//  ThemeManager.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 06/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

final class ThemeManager {
    
    static let shared = ThemeManager()
    
    var theme: Theme = DefaultTheme() {
        didSet {
            applyTheme()
        }
    }
    
    private let listeners = NSHashTable<AnyObject>.weakObjects()
    private let writeQueue = DispatchQueue.init(label: "theme.writequeue", qos: DispatchQoS.default)
    
    private init() { }
    
    func addThemeable(themable: Themeable) {
        writeQueue.async { [unowned self] in
            guard !self.listeners.contains(themable) else { return }
            self.listeners.add(themable)
        }
        
        themable.applyTheme(theme: theme)
    }
    
    private func applyTheme() {
        // Update styles via UIAppearance
        UINavigationBar.appearance().isTranslucent = theme.navigationBarTranslucent
        UINavigationBar.appearance().barTintColor = theme.navigationBarTintColor
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: theme.navigationTitleColor,
            NSAttributedString.Key.font: theme.navigationTitleFont
        ]
        
        // The tintColor will trickle down to each view
        if let window = UIApplication.shared.windows.first {
            window.tintColor = theme.tintColor
        }
        
        // Update each listener. The type cast is needed because allObjects returns [AnyObject]
        listeners.allObjects
            .compactMap { $0 as? Themeable }
            .forEach { $0.applyTheme(theme: theme) }
    }
}
