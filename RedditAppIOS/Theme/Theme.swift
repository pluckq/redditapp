//
//  Theme.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 05/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit
import SwiftRichString

protocol Theme {
    
    var backgroundColor: UIColor { get }
    var tintColor: UIColor { get }
    var navigationBarTintColor: UIColor? { get }
    var navigationBarTranslucent: Bool { get }
    
    var navigationTitleFont: UIFont { get }
    var navigationTitleColor: UIColor { get }
    
    var headlineFont: UIFont { get }
    var headlineColor: UIColor { get }
    
    var bodyTextFont: UIFont { get }
    var bodyTextColor: UIColor { get }
    
    var smallTextFont: UIFont { get }
    var smallTextColor: UIColor { get }
    
    var headlineTextStyle: Style { get }
    var bodyTextStyle: Style { get }
    var smallTextStyle: Style { get }
}

extension Theme {
    
    //MARK: Attributed string
    
    var headlineTextStyle: Style {
        let attributes: [NSAttributedString.Key : Any] = [.foregroundColor: headlineColor,
                                                          .font: headlineFont]
        return Style(dictionary: attributes)
    }
    
    var bodyTextStyle: Style {
        let attributes: [NSAttributedString.Key : Any] = [.foregroundColor: bodyTextColor,
                                                          .font: bodyTextFont]
        return Style(dictionary: attributes)
    }
    
    var smallTextStyle: Style {
        let attributes: [NSAttributedString.Key : Any] = [.foregroundColor: smallTextColor,
                                                          .font: smallTextFont]
        return Style(dictionary: attributes)
    }
    
    //MARK: Texture
    
    public func applyBackgroundColor(nodes: [ASDisplayNode]) {
        nodes.forEach {
            $0.backgroundColor = backgroundColor
        }
    }
    
    func applyHeadlineTextStyle(nodes: [(node: ASTextNode, text: String?)]) {
        nodes.forEach {
            $0.node.attributedText = $0.text?.set(style: headlineTextStyle)
        }
    }
    
    func applyBodyTextStyle(nodes: [(node: ASTextNode, text: String?)]) {
        nodes.forEach {
            $0.node.attributedText = $0.text?.set(style: bodyTextStyle)
        }
    }
    
    func applySmallTextStyle(nodes: [(node: ASTextNode, text: String?)]) {
        nodes.forEach {
            $0.node.attributedText = $0.text?.set(style: smallTextStyle)
        }
    }
    
    func applyTextButtonStyle(nodes: [(node: ASButtonNode, title: String)]) {
        nodes.forEach {
            $0.node.setTitle($0.title, with: smallTextFont, with: bodyTextColor, for: .normal)
            $0.node.setTitle($0.title, with: smallTextFont, with: .lightGray, for: .highlighted)
        }
    }
    
    //MARK: UIKit
    
    func applyBackgroundColor(views: [UIView]) {
        views.forEach {
            $0.backgroundColor = backgroundColor
        }
    }
    
    func applyHeadlineStyle(labels: [UILabel]) {
        labels.forEach {
            $0.font = headlineFont
            $0.textColor = headlineColor
        }
    }
    
    func applyBodyTextStyle(labels: [UILabel]) {
        labels.forEach {
            $0.font = bodyTextFont
            $0.textColor = bodyTextColor
        }
    }
    
    func applySmallTextStyle(labels: [UILabel]) {
        labels.forEach {
            $0.font = smallTextFont
            $0.textColor = smallTextColor
        }
    }
}
