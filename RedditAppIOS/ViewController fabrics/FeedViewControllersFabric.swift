//
//  FeedViewControllersFabric.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 14/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

protocol FeedViewControllersFabricProtocol: class {
    func createFeedViewControllerModule() -> FSNavigationController
}

final class FeedViewControllersFabric: FeedViewControllersFabricProtocol {

    func createFeedViewControllerModule() -> FSNavigationController {
        let feedModelController = FeedPlainModelController()
        let feedDataSource = FeedPlainDataSource()
        let feedDependencies = FeedViewControllerDependencies(modelController: feedModelController, dataSource: feedDataSource)
        
        let feedVC = FeedViewController(dependencies: feedDependencies)
        
        let feedNC = FSNavigationController(rootViewController: feedVC)
        feedNC.tabBarItem = UITabBarItem(title: "Feed", image: nil, tag: 0)
        
        return feedNC
    }
}
