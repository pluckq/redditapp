//
//  FeedCellTextView.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 08/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

final class FeedTextView: ASDisplayNode, Themeable {

    private lazy var titleLabel: ASTextNode = ASTextNode()
    private lazy var textLabel: ASTextNode = {
        let label = ASTextNode()
        label.maximumNumberOfLines = 5
        return label
    }()
    
    private let viewModel: FeedTextViewModel
    
    init(viewModel: FeedTextViewModel) {
        self.viewModel = viewModel
        
        super.init()
        
        automaticallyManagesSubnodes = true
        ThemeManager.shared.addThemeable(themable: self)
    }
    
    func applyTheme(theme: Theme) {
        theme.applyHeadlineTextStyle(nodes: [(titleLabel, viewModel.title)])
        theme.applyBodyTextStyle(nodes: [(textLabel, viewModel.text)])
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let stackLayout = ASStackLayoutSpec(direction: .vertical,
                                            spacing: 15,
                                            justifyContent: .center,
                                            alignItems: .start,
                                            children: [titleLabel, textLabel])
        
        stackLayout.style.width = ASDimensionMakeWithFraction(1.0)
        
        return stackLayout
    }
}
