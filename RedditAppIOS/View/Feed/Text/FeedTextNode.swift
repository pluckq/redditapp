//
//  FeedTextCollectionViewCell.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 03/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

final class FeedTextNode: ASCellNode, FeedNodeProtocol {
    
    typealias ContentView = FeedTextView
    typealias TextCellDependencies = HasFeedCellTopViewModel & HasFeedCellBottomViewModel & HasFeedCellTextViewModel
    
    var topView: FeedTopView
    var bottomView: FeedBottomView
    var contentView: FeedTextView
    
    init(dependencies: TextCellDependencies) {
        contentView = FeedTextView(viewModel: dependencies.textViewModel)
        topView = FeedTopView(viewModel: dependencies.topViewModel)
        bottomView = FeedBottomView(viewModel: dependencies.bottomViewModel)
        
        super.init()
        
        automaticallyManagesSubnodes = true
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let stackLayout = ASStackLayoutSpec(direction: .vertical,
                                            spacing: 8,
                                            justifyContent: .center,
                                            alignItems: .start,
                                            children: [topView, contentView, bottomView])
        
        stackLayout.style.width = ASDimensionMakeWithFraction(1.0)
        
        return  ASInsetLayoutSpec(insets: UIEdgeInsets(top: 15, left: 15, bottom: 0, right: 15), child: stackLayout)
    }
}
