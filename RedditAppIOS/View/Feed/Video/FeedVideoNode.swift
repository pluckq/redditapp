//
//  FeedVideoNode.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 07/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

final class FeedVideoNode: ASCellNode, FeedNodeProtocol {

    typealias ContentView = FeedVideoView
    typealias VideoCellDependencies = HasFeedCellTopViewModel & HasFeedCellBottomViewModel & HasFeedCellVideoViewModel
    
    var topView: FeedTopView
    var bottomView: FeedBottomView
    var contentView: FeedVideoView
    
    
    init(dependencies: VideoCellDependencies) {
        topView = FeedTopView(viewModel: dependencies.topViewModel)
        bottomView = FeedBottomView(viewModel: dependencies.bottomViewModel)
        contentView = FeedVideoView()
        
        super.init()
        
        automaticallyManagesSubnodes = true
    }
}
