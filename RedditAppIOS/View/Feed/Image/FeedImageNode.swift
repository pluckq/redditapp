//
//  FeedImageNode.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 07/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

final class FeedImageNode: ASCellNode, FeedNodeProtocol {
    
    typealias ContentView = FeedImageView
    typealias ImageCellDependencies = HasFeedCellTopViewModel & HasFeedCellBottomViewModel & HasFeedCellImageViewModel
    
    var topView: FeedTopView
    var bottomView: FeedBottomView
    var contentView: FeedImageView

    init(dependencies: ImageCellDependencies) {
        topView = FeedTopView(viewModel: dependencies.topViewModel)
        bottomView = FeedBottomView(viewModel: dependencies.bottomViewModel)
        contentView = FeedImageView(viewModel: dependencies.imageViewModel)
        
        super.init()
        
        automaticallyManagesSubnodes = true
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let stackLayout = ASStackLayoutSpec(direction: .vertical,
                                            spacing: 8,
                                            justifyContent: .center,
                                            alignItems: .start,
                                            children: [topView, contentView, bottomView])
        
        stackLayout.style.width = ASDimensionMakeWithFraction(1.0)
        
        return  ASInsetLayoutSpec(insets: UIEdgeInsets(top: 15, left: 15, bottom: 0, right: 15), child: stackLayout)
    }
}
