//
//  FeedCellImageView.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 09/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

final class FeedImageView: ASDisplayNode {

    private lazy var imageNode: ASNetworkImageNode = ASNetworkImageNode()
    private let viewModel: FeedImageViewModel
    
    init(viewModel: FeedImageViewModel) {
        self.viewModel = viewModel
        
        super.init()
        
        imageNode.url = viewModel.image
        imageNode.shouldRenderProgressImages = true
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        imageNode.style.preferredSize = CGSize(width: 100, height: 200)
        return ASWrapperLayoutSpec(layoutElement: imageNode)
    }
}
