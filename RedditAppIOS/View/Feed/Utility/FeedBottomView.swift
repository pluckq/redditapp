//
//  FeedCellBottomView.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 07/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

final class FeedBottomView: ASDisplayNode, Themeable {
    
    private lazy var txtUpvotes: ASTextNode = ASTextNode()
    private lazy var txtComments: ASTextNode = ASTextNode()
    private lazy var txtAuthor: ASTextNode = ASTextNode()
    private lazy var txtSubreddit: ASTextNode = ASTextNode()
    private lazy var imgSubreddit: ASNetworkImageNode = ASNetworkImageNode()
    private lazy var btnUpvote: ASButtonNode = ASButtonNode()
    private lazy var btnDownvote: ASButtonNode = ASButtonNode()
    
    private let viewModel: FeedBottomViewModel
    

    init(viewModel: FeedBottomViewModel) {
        self.viewModel = viewModel
        
        super.init()
        
        automaticallyManagesSubnodes = true
        ThemeManager.shared.addThemeable(themable: self)
    }
    
    func applyTheme(theme: Theme) {
        theme.applySmallTextStyle(nodes: [(txtUpvotes, viewModel.upvotes), (txtAuthor, viewModel.author), (txtComments, viewModel.commentsCount), (txtSubreddit, viewModel.subreddit)])
        theme.applyTextButtonStyle(nodes: [(btnUpvote, "Up"), (btnDownvote, "Down")])
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        
        let infoLayout = ASStackLayoutSpec(direction: .horizontal,
                                           spacing: 8,
                                           justifyContent: .spaceBetween,
                                           alignItems: .notSet,
                                           children: [imgSubreddit, txtSubreddit, txtAuthor])
        
        let infoNumbersLayout = ASStackLayoutSpec(direction: .horizontal,
                                                  spacing: 8,
                                                  justifyContent: .spaceBetween,
                                                  alignItems: .notSet,
                                                  children: [txtUpvotes, txtComments])
        
        let infoRootLayout = ASStackLayoutSpec(direction: .horizontal,
                                               spacing: 0,
                                               justifyContent: .spaceBetween,
                                               alignItems: .stretch,
                                               children: [infoLayout, infoNumbersLayout])
        
        infoRootLayout.style.width = ASDimensionMakeWithFraction(1.0)
        
        let buttonsLayout = ASStackLayoutSpec(direction: .horizontal,
                                           spacing: 8,
                                           justifyContent: .spaceBetween,
                                           alignItems: .notSet,
                                           children: [btnUpvote, btnDownvote])
        
        let rootLayout = ASStackLayoutSpec(direction: .vertical,
                                           spacing: 8,
                                           justifyContent: .spaceBetween,
                                           alignItems: .notSet,
                                           children: [infoRootLayout, buttonsLayout])
        
        rootLayout.style.width = ASDimensionMakeWithFraction(1.0)
        
        return rootLayout
    }
}
