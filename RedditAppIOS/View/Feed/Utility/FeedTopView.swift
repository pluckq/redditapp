//
//  FeedTopView.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 07/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

final class FeedTopView: ASDisplayNode, Themeable {
    
    private lazy var dateLabel: ASTextNode = ASTextNode()
    private lazy var btnOptions: ASButtonNode = ASButtonNode()
    
    private let viewModel: FeedTopViewModel

    init(viewModel: FeedTopViewModel) {
        self.viewModel = viewModel
        
        super.init()
        
        automaticallyManagesSubnodes = true
        ThemeManager.shared.addThemeable(themable: self)
    }
    
    func applyTheme(theme: Theme) {
        theme.applyTextButtonStyle(nodes: [(btnOptions, "Options")])
        theme.applySmallTextStyle(nodes: [(dateLabel, viewModel.date)])
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let stackLayout = ASStackLayoutSpec(direction: .horizontal,
                                            spacing: 0,
                                            justifyContent: .spaceBetween,
                                            alignItems: .notSet,
                                            children: [dateLabel, btnOptions])
        
        stackLayout.style.width = ASDimensionMakeWithFraction(1.0)
        
        return stackLayout
    }
}
