//
//  RedditOauth.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 16/08/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Foundation

protocol RedditOauthDelegate: class {
    func didRecieveResponse()
}

final class RedditOauth {

    func registerAuthWith(url: URL) {
        if url.scheme == "reddfeed" {
            if let token = url.query?.components(separatedBy: "&").filter({$0.contains("code=")}).first {
                print(token)
            }
        }
    }
}
