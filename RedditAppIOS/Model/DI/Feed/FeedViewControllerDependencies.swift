//
//  FeedViewControllerDependencies.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 03/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

struct FeedViewControllerDependencies: HasFeedModelController, HasFeedDataSource {
    let modelController: FeedModelControllerProtocol
    let dataSource: FeedDataSource
}
