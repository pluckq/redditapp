//
//  FeedTextNodeDependencies.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 13/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

struct FeedTextNodeDependencies: HasFeedCellTopViewModel, HasFeedCellBottomViewModel, HasFeedCellTextViewModel {
    let textViewModel: FeedTextViewModel
    let topViewModel: FeedTopViewModel
    let bottomViewModel: FeedBottomViewModel
}
