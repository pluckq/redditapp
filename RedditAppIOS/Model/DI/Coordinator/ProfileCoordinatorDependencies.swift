//
//  ProfileCoordinatorDependencies.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 31/08/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Foundation

struct ProfileCoordinatorDependencies: HasTabbarRouter, HasFeedViewControllersFabric {
    var tabbarRouter: TabbarRouter
    var feedViewControllersFabric: FeedViewControllersFabricProtocol
}

struct TabbarCoordinatorDependencies: HasTabbarRouter {
    let tabbarRouter: TabbarRouter
}
