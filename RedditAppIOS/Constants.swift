//
//  Constants.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 31/08/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Foundation

typealias VoidBlock = (() -> ())?
typealias Observable<T> = ((T) -> ())?
