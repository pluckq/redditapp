//
//  FeedCellTextViewModel.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 09/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Foundation

struct FeedTextViewModel {
    let title: String?
    let text: String?
}
