//
//  LinkViewModel.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 17/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Foundation

struct LinkViewModel {
    let previewImage: URL?
    let link: URL?
}
