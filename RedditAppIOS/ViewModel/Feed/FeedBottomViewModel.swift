//
//  FeedCellBottomViewModel.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 13/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Foundation

struct FeedBottomViewModel {
    let author: String?
    let subreddit: String?
    let upvotes: String?
    let commentsCount: String?
}
