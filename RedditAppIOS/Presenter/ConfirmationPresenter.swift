//
//  ConfirmationPresenter.swift
//  RedditAppIOS
//
//  Created by vadim vitvickiy on 17/09/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

struct ConfirmationPresenter {
    
    enum Outcome {
        case accepted
        case rejected
    }
    
    let question: String
    let acceptTitle: String
    let rejectTitle: String
    let handler: (Outcome) -> Void
    
    func present(in viewController: UIViewController) {
        let alert = UIAlertController(
            title: nil,
            message: question,
            preferredStyle: .alert
        )
        
        let rejectAction = UIAlertAction(title: rejectTitle, style: .cancel) { _ in
            self.handler(.rejected)
        }
        
        let acceptAction = UIAlertAction(title: acceptTitle, style: .default) { _ in
            self.handler(.accepted)
        }
        
        alert.addAction(rejectAction)
        alert.addAction(acceptAction)
        
        viewController.present(alert, animated: true)
    }
}
