//
//  AppDelegate.swift
//  RedditAppMacOS
//
//  Created by vadim vitvickiy on 14/08/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

